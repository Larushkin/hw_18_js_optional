const earth = {
    name: 'Continent name',
    continent: {
        Eurasia: [
            {
                country: 'Israel',
                capital: 'Jerusalem',
            },
            {
                country: 'Ukraine',
                capital: 'Kiev',
            },
            {
                country: 'Poland',
                capital: 'Warsaw',
            },
            {
                country: 'Spain',
                capital: 'Madrid',
            },
            {
                country: 'Germany',
                capital: 'Berlin',
            },
        ],
        Africa: [
            {
                country: 'Republic of Angola',
                capital: 'Luanda',
            },
            {
                country: 'Republic of Botswana',
                capital: 'Gaborone',
            },
            {
                country: 'Republic of Djibouti',
                capital: 'Djibouti',
            },
        ],
        North_America: [
            {
                country: 'USA',
                capital: 'Washington DC',
            },
            {
                country: 'Canada',
                capital: 'Ottawa',
            },
            {
                country: 'Mexico',
                capital: 'Mexico city',
            },
        ],
    }
}

console.log(earth);

function objectFullClone (obj) {
    const objClone = {};
    for (let key in obj) {
        if (typeof obj[key] !== 'array' && obj[key] !== null) {
            objClone[key] = obj[key];
        } else {
            objClone[key] = objectFullClone(obj[key]);
        }
    }

    return objClone;
}

const earth2 = objectFullClone(earth);

earth2.name = 'Test';

console.log(earth2);
